package com.minhthuc.Model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by minhthuc on 3/29/2017.
 */
@XmlRootElement(name = "model")
public class Model {

    private int user_id;
    private String password;
    private int instagram_id;
    private String user_name;
    private int follows;
    private String followed_by;
    private String gender;
    private int fee_tv;
    private int fee_web;
    private int fee_salon;
    private int id_init;
    private int id_album;

    public Model() {
    }

    public int getUser_id() {
        return user_id;
    }

    @XmlElement(name = "user_id")
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getInstagram_id() {
        return instagram_id;
    }

    @XmlElement(name = "instagram_id")
    public void setInstagram_id(int instagram_id) {
        this.instagram_id = instagram_id;
    }

    public String getUser_name() {
        return user_name;
    }

    @XmlElement(name = "username")
    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getFollows() {
        return follows;
    }

    @XmlElement(name = "follow")
    public void setFollows(int follows) {
        this.follows = follows;
    }

    public String getFollowed_by() {
        return followed_by;
    }

    @XmlElement(name = "followed_by")
    public void setFollowed_by(String followed_by) {
        this.followed_by = followed_by;
    }

    public String getGender() {
        return gender;
    }

    @XmlElement(name = "Gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getFee_tv() {
        return fee_tv;
    }

    @XmlElement(name = "fee_tv")
    public void setFee_tv(int fee_tv) {
        this.fee_tv = fee_tv;
    }

    public int getFee_web() {
        return fee_web;
    }

    @XmlElement(name = "fee_web")
    public void setFee_web(int fee_web) {
        this.fee_web = fee_web;
    }

    public int getFee_salon() {
        return fee_salon;
    }

    @XmlElement(name = "fee_salon")
    public void setFee_salon(int fee_salon) {
        this.fee_salon = fee_salon;
    }

    public int getId_init() {
        return id_init;
    }

    @XmlElement(name = "id_init")
    public void setId_init(int id_init) {
        this.id_init = id_init;
    }

    public int getId_album() {
        return id_album;
    }

    @XmlElement(name = "id_album")
    public void setId_album(int id_album) {
        this.id_album = id_album;
    }

    public String getPassword() {
        return password;
    }

    @XmlElement
    public void setPassword(String password) {
        this.password = password;
    }
}
