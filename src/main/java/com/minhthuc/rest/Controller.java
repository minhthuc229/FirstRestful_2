package com.minhthuc.rest;

import com.minhthuc.Connect.ConnectDatabase;
import com.minhthuc.Model.Model;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by minhthuc on 3/31/2017.
 */
@Path("/model")
public class Controller {
    @GET
    @Path("/json")
    @Produces(MediaType.APPLICATION_JSON)
    public Model getModel() throws SQLException, ClassNotFoundException {
        ConnectDatabase connect = new ConnectDatabase();
        Connection con = connect.connect();
        Model m = new Model();
            ResultSet rs = connect.Querry(con,"Select * from model");
            while (rs.next()){
                m.setUser_id(rs.getInt(1));
                m.setInstagram_id(rs.getInt(2));
                m.setUser_name(rs.getString(3));
                m.setPassword(rs.getString(4));
                m.setFollows(rs.getInt(5));
                m.setFollowed_by(rs.getString(6));
                m.setGender(rs.getString(7));
                m.setFee_tv(rs.getInt(8));
                m.setFee_web(rs.getInt(9));
                m.setFee_salon(rs.getInt(10));
                m.setId_init(rs.getInt(11));
                m.setId_album(rs.getInt(12));
//            list.add(m);
            }
            con.close();
            return m;
    }
}
