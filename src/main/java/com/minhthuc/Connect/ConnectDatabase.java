package com.minhthuc.Connect;

import java.sql.*;

/**
 * Created by minhthuc on 3/31/2017.
 */
public class ConnectDatabase {
    private final String database = "model";
    private final String username = "root";
    private final String password = "123456";
    public Connection connect() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/model", "root", "123456");
        return con;
    }
    public ResultSet Querry(Connection con,String Querry) throws SQLException, ClassNotFoundException {
        con = this.connect();
        Statement stml = con.createStatement();
        ResultSet rs = stml.executeQuery(Querry);
//        con.close();
        return rs;
    }
}
